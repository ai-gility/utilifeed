package main

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"time"
)

//FluidData - structure describing fluid
type FluidData struct {
	Datetime    time.Time
	Flow        float64
	Temperature float64
}

//AggregateData - aggregates data
func AggregateData(data []FluidData) FluidData {
	var agg FluidData
	agg.Datetime = data[0].Datetime
	var energy = 0.0
	for i := 0; i < len(data); i++ {
		agg.Flow = agg.Flow + data[i].Flow
		energy = energy + data[i].Flow*data[i].Temperature
	}
	agg.Temperature = energy / agg.Flow
	return agg
}

//LoadDataArray - loads data from file
func LoadDataArray(file string, timeFormat string) []FluidData {
	//read file
	//todo: any better way to read csv file in go?
	var r *csv.Reader
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	r = csv.NewReader(f)
	var rows [][]string
	rows, err = r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	//parse
	var nRows = len(rows)
	data := make([]FluidData, nRows-1)
	for j, row := range rows {
		if j > 0 { //skip header row
			data[j-1].Datetime, _ = time.Parse(timeFormat, row[0])
			data[j-1].Flow, _ = strconv.ParseFloat(row[1], 64)
			data[j-1].Temperature, _ = strconv.ParseFloat(row[2], 64)
		}
	}
	return data
}

//SaveDataArray - saves array of data to file
func SaveDataArray(stream []FluidData, fileName string, timeFormat string) {
	nRows := len(stream)
	csvData := make([][]string, nRows+1)
	//header
	csvData[0] = []string{"datetime", "flow", "temperature"}
	//data
	for i := 0; i < nRows; i++ {
		csvData[i+1] = make([]string, 3)
		csvData[i+1][0] = stream[i].Datetime.Format(timeFormat)
		csvData[i+1][1] = strconv.FormatFloat(stream[i].Flow, 'f', 2, 64)
		csvData[i+1][2] = strconv.FormatFloat(stream[i].Temperature, 'f', 2, 64)
	}
	//write to file
	f, err := os.Create(fileName)
	if err != nil {
		log.Fatal(err)
	}
	w := csv.NewWriter(f)
	err = w.WriteAll(csvData)
	if err != nil {
		log.Fatal(err)
	}

	return
}
