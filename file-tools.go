package main

import (
	"io/ioutil"
	"log"
	"strings"
)

//GetFiles - gets all files in directory that match mask and extention
func GetFiles(dir string, mask string, extention string) []string {
	//get all files
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	//select only files of interest
	var inputFiles []string
	for _, f := range files {
		//todo: change from 'contains' to 'starts with' and 'ends with' or go with 'regex'
		if f.IsDir() == false && strings.Contains(f.Name(), mask) && strings.Contains(f.Name(), extention) {
			inputFiles = append(inputFiles, f.Name())
		}
	}
	return inputFiles
}
