package main

import "log"

const timeFormat = "2006-01-02 15:04:05"
const inputDir = "./input/"
const inputFileMask = "stream_"
const inputFileExtention = "csv"
const outputFile = "output/stream_aggregate.csv"

func main() {
	//get input files
	files := GetFiles(inputDir, inputFileMask, inputFileExtention)
	log.Printf("%d files to process in '%s'", len(files), inputDir)

	//load files
	var fileCount = len(files)
	dfs := make([][]FluidData, fileCount)
	for i, file := range files {
		dfs[i] = LoadDataArray(inputDir+file, timeFormat)
		log.Printf("'%s' successfully loaded", file)
	}

	//aggragate
	// todo: check for same-time-ticks
	var aggData []FluidData
	for i := 0; i < len(dfs[0]); i++ {
		var momentData []FluidData
		for f := 0; f < fileCount; f++ {
			momentData = append(momentData, dfs[f][i])
		}
		aggData = append(aggData, AggregateData(momentData))
	}
	log.Printf("data aggregated")

	//save
	SaveDataArray(aggData, outputFile, timeFormat)
	log.Printf("result saved to '%s'", outputFile)

	return
}
