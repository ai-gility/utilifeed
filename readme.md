# Further enhancements 
## Project structure
- make it more go-style with package-git-structure
- move tests to separate folder
## Test 
- extract convertion of data-array to and from 'csv' ([][]string) in load/save to separate functions and test them
- add test for proper input filename ('csv' extention, any string-mask in name)
## Program
- main weak point is aggragation of flow-data based on assumption that csv files are correct and 'synchronized' in terms of same date-time-ticks