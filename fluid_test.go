package main

import (
	"testing"
	"time"
)

//TestAggregation - tests correctness of aggregate function
func TestAggregation(t *testing.T) {
	var f1 = FluidData{time.Now(), 1, 70.00}
	var f2 = FluidData{time.Now(), 2, 40.00}
	var f3 = FluidData{time.Now(), 3, 50.00}
	var data = []FluidData{f1, f2}
	agg := AggregateData(data)
	if agg.Flow != f3.Flow || agg.Temperature != f3.Temperature {
		t.Errorf("Incorrect aggregation function")
	}
}
